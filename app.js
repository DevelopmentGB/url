let protocol = 'http://'
let languageCheckboxes = document.getElementsByClassName('language-checkbox');
let typeCheckboxes = document.getElementsByClassName('type-checkbox');



const urlCleaner = () => {
    let a = document.querySelector('a');
    let generatedUrl = document.getElementsByClassName("generated-url");

    if (results.contains(a)) {
        [...generatedUrl].map(n => n && n.remove());
    }
}


 var urlCreate = () => {
    const BASE = document.getElementById('base').value;
    let results = document.getElementById('results');
    let mode = document.querySelector('input[name="dev-radio"]:checked').value;
    let devToolsCheckboxes = document.querySelectorAll('input[name="dev-checkbox"]:checked');


    urlCleaner();


    for (let typeCheckbox of typeCheckboxes) {
        if (typeCheckbox.checked) {

            for (let languageCheckbox of languageCheckboxes) {
                if (languageCheckbox.checked) {

                    let url = `${ protocol + BASE + typeCheckbox.value + mode + languageCheckbox.value }`;
                    if (devToolsCheckboxes != null) {
                        for (let devToolsCheckbox of devToolsCheckboxes) {
                            devToolsCheckbox = devToolsCheckbox.value;
                            console.log(devToolsCheckbox);
                            url += `${ devToolsCheckbox }`;
                        }
                    }
                    let anchor = document.createElement('a');
                    anchor.innerHTML = `${ url }`;
                    anchor.href = anchor.innerHTML;
                    anchor.classList.add('generated-url');
                    results.appendChild(anchor);
                }
            }
        }

    }

}
